package ee.bsc.valiit;

public class EX3 {


    public static final double VAT = 1.2; // konstant. ei muutu.


    public static double addVat(double duubel) {
        return duubel * VAT;
    }

    public static void main(String[] args) {
        addVat(100.0);
        System.out.println("vat: " + addVat(100.0));



    }


}
