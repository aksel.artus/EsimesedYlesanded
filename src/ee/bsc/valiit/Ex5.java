package ee.bsc.valiit;

public class Ex5 {


    public static void main(String[] args) {
        String gender = deriveGendre("47212210231");
        System.out.println("Gender is " + gender);
    }

    public static String deriveGendre(String identificationNumber) {

        return (Character.getNumericValue(identificationNumber.charAt(0))%2 == 0) ? "F" : "M";

        /*
        if (Integer.getInteger(identificationNumber.substring(0, 1))%2 == 0) {
            return "F";
        } else {
            return "M";
        }

        if (identificationNumber.startsWith("4") ||
        identificationNumber.startsWith("6")) {
            return "F";
        } else if (identificationNumber.startsWith("3") ||
        identificationNumber.startsWith("5")) {
            return "M";
        } else {
            return "Unknown";
        }

        return identificationNumber.startsWith("4") ||
                identificationNumber.startsWith("6") ?
                "F" : "M";
    */
    }

}
