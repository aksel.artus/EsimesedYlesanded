package ee.bcs.valiit.kordamine;

public class Ex7 {

    public static int deriveBirthYear(String idKood){

        char gender = idKood.charAt(0);
        StringBuilder aasta = new StringBuilder();
        switch (gender) {
            case '1':
            case '2':
                aasta.append("18");
                break;
            case '3':
            case '4':
                aasta.append("19");
                break;
            case '5':
            case '6':
                aasta.append("20");
                break;
            default:
                break;
        }
        String kumnend = idKood.substring(1, 3);
        aasta.append(kumnend);
        int result = Integer.parseInt(aasta.toString());
        return result;



    }

    public static void main(String[] args) {
        int resultaat = deriveBirthYear("39005230024");

        System.out.println(resultaat);

    }



}
