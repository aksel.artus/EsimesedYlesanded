package ee.bsc.valiit;

public class Ex4 {
    public static int[] Different(int a, int b, boolean asc) {
        int[] array = new int[2];

        if (asc == true) { // kui tingimuslauses on ainult üks rida siis pole vaja sulge.
            array[0] = a;
            array[1] = b;

        }else {
            array[0] = b;
            array[1] = a;

        }
        return array;


    }

    public static void main(String[] args) {

        int[] array = Different(2, 3,true);

        System.out.println(String.format("array: %d %d", array[0], array[1]));

        array = Different(2,3, false);
        System.out.println(String.format("array: %d %d", array[0], array[1]));





    }





}
